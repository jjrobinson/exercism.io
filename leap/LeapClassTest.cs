﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;

namespace Exercism.io.leap
{
    [TestFixture]
    class LeapClassTest
    {
        [Test]
        public void TestCommonLeap1()
        {
            //setup
            LeapClass leapClass = new LeapClass();

            //act
            bool test1 = leapClass.IsLeap(2000);

            //Assert
            Assert.AreEqual(true, test1, "Incorrectly saying that 2000 is NOT a leap year.");

        }

        [Test]
        public void TestCommonLeap2()
        {
            //setup
            LeapClass leapClass = new LeapClass();

            //act
            bool test1 = leapClass.IsLeap(1900);

            //Assert
            Assert.AreEqual(false, test1, "Incorrectly saying that 1900 is a leap year.");

        }

        [Test]
        public void TestCommonLeap3()
        {
            //setup
            LeapClass leapClass = new LeapClass();

            //act
            bool test1 = leapClass.IsLeap(1999);

            //Assert
            Assert.AreEqual(false, test1, "Incorrectly saying that 1999 is a leap year.");

        }

    }
}
