﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;

public class LeapClass
{
    public bool IsLeap(int year)
    {
        
        if (year % 4 == 0)
        { // possibly a leap year
            if (year % 100 == 0)
                if (year % 400 == 0)
                    return true; // divisible by 100 & 400!
                else return false; // not divisible by 400
            else return true; // not divisible by 100 but was by 4
        }
        else
            return false; // not divisible by 4, no hope of leap year
        /*
        return (year % 4 == 0)
            && (year % 100 != 0)
            || (year % 400 == 0)
            ;

         */
    }


}
