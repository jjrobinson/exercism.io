﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercism.io.bob
{
    class Bob
    {
        string chill = "Whoa, chill out!";
        string whatever = "Whatever.";
        string sure = "Sure.";
        string fine = "Fine. Be that way!";

        public string Hey(string phrase)
        {
            //return right away if the phrase is just empty white space.
            if (phrase.Length == 0 || phrase.Trim().Length == 0)
                return fine;

            char[] charArray = phrase.ToCharArray(0, phrase.Length);
            string itIsA = null;
            string lastCharIsA = null;
            bool allCapsLetters = false;
            bool lowerCaseFound = false;
            bool numberFound = false;
            bool questionMarkFound = false;
            bool bangFound = false;
            bool periodFound = false;
            bool onlyLetters = true;
            bool moreThan1Word = false;
            bool oneWordOrMore = false;
            bool punctFound = false;
            string lastPunct = null;
            int numberOfWords = 0;
            char lastChar = ' ';

            foreach (char c in charArray)
            {
                //saving the last character
                lastCharIsA = itIsA;
                //getting what the new character is
                itIsA = whatIsIt(c);
                if (itIsA.Equals("punct"))
                {
                    punctFound = true;
                    //if we hit a punctuation after anything else but another punctuation
                    //then that is a new word
                    if(!lastCharIsA.Equals("punct"))
                        numberOfWords++;

                    //lets find out what kind of punctuation this is
                    //and over write the generic punct with the real kind.
                    itIsA = whatKindOfPunct(c);
                    switch (c)
                    {
                        case '.':
                            periodFound = true;
                            lastPunct = itIsA;
                            break;
                        case '?':
                            questionMarkFound = true;
                            lastPunct = itIsA;
                            break;
                        case '!':
                            bangFound = true;
                            lastPunct = itIsA;
                            break;
                        default:
                            break;
                    }
                } // end of punctuation loop


                if (itIsA.Equals("number"))
                {
                    numberFound = true;
                    onlyLetters = false;
                }



                //test for all caps so far in the string
                //assuning start of all caps
                if ((itIsA.Equals("upper")) && (lowerCaseFound == false))
                {
                    allCapsLetters = true;
                }
                else if (itIsA.Equals("lower"))
                {
                    allCapsLetters = false;
                    lowerCaseFound = true;
                }

                //saving characters for next loop comparison
                lastChar = c;
                lastCharIsA = itIsA;
            // end of string character by character loop
            }

            //if yelling in all caps with or with out numbers
            if (allCapsLetters)
                if (punctFound)
                {
                    //minimum phrase for yelling with punct is 3 characters
                    if (phrase.Length > 2)
                        return chill;
                    //with punctuation but only 2 characters is nothing.
                    else
                        return whatever;
                }
                //punctuation not found
                else return chill;
            //not all caps letters, so it cannot be yelling

            else if (questionMarkFound && lastCharIsA.Equals("question"))
                return sure;

            return whatever;
        }


        /// <summary>
        /// returns what kind of character is passed to it
        /// </summary>
        /// <param name="it">a single character in english alphabet or punctuation, space, symbol</param>v
        /// <returns>String description of character</returns>
        public string whatIsIt(char it)
        {
            if(Char.IsDigit(it))
                return "number";
            else if (Char.IsLetter(it))
            {
                if(Char.IsLower(it))
                    return "lower";
                else return "upper";
            }
            else if (Char.IsControl(it))
                return "control";
            else if (Char.IsPunctuation(it))
                return "punct";
            else if (Char.IsWhiteSpace(it))
                return "space";
            else return "unknown";

        }

        /// <summary>
        /// Returns the specific kind of punctuation of a char if know.
        /// </summary>
        /// <param name="it">char it - the character to be examined</param>
        /// <returns>String telling you what kind of punctuation character it is.</returns>
        public string whatKindOfPunct(char it)
        {
            if (it == '!')
                return "bang";
            else if (it == '?')
                return "question";
            else if (it == '.')
                return "period";
            else if (it == ',')
                return "comma";
            else return "other punct";
        }
    }
}
