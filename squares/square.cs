﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;


namespace Exercism.io.square
{

    public class Square
    {
        public bool isSquareFast(int number)
        {
            Console.WriteLine("The Square root of " + number + " is..." + Math.Sqrt(number));
            if ((Math.Sqrt(number) % 1) == 0)
                return true;
            else return false;
        }

        public bool isSquareSlow(int number)
        {
            Console.WriteLine("The Square root of " + number + " is..." + Math.Sqrt(number));
            if ((Math.Sqrt(number) % 1) == 0)
                return true;
            else return false;
        }


    }
}