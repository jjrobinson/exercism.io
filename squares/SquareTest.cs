using NUnit.Framework;

namespace Exercism.io.square
{
    [TestFixture]
    public class SquareTest
    {

        Square number2 = new Square();

        [Test]
        public void Valid_4()
        {
            Assert.That(number2.isSquareFast(4), Is.True);
        }


        [Test]
        public void Invalid_5()
        {
            Assert.That(number2.isSquareFast(5), Is.False);
        }

        [Test]
        public void Valid_16()
        {
            Assert.That(number2.isSquareFast(16), Is.True);
        }

        [Test]
        public void Invalid_1999()
        {
            Assert.That(number2.isSquareFast(1999), Is.False);
        }

        [Test]
        public void Valid_4_slow()
        {
            Assert.That(number2.isSquareSlow(4), Is.True);
        }


        [Test]
        public void Invalid_5_slow()
        {
            Assert.That(number2.isSquareSlow(5), Is.False);
        }

        [Test]
        public void Valid_16_slow()
        {
            Assert.That(number2.isSquareSlow(16), Is.True);
        }

        [Test]
        public void Invalid_1999_slow()
        {
            Assert.That(number2.isSquareSlow(1999), Is.False);
        }
    }
}